package params

// UsePulseChainNetworkConfig uses the PulseChain beacon chain mainnet network config.
func UsePulseChainNetworkConfig() {
	// TODO
	UsePulseChainTestnetV3NetworkConfig()
}

// PulseChainConfig defines the config for the PulseChain beacon chain mainnet.
func PulseChainConfig() *BeaconChainConfig {
	// TODO
	return PulseChainTestnetV3Config()
}
