package params

const (
	DevnetName              = "devnet"
	EndToEndName            = "end-to-end"
	EndToEndMainnetName     = "end-to-end-mainnet"
	InteropName             = "interop"
	MainnetName             = "mainnet"
	PulseChainName          = "pulsechain-mainnet"
	MainnetTestName         = "mainnet-test"
	MinimalName             = "minimal"
	PraterName              = "prater"
	GoerliName              = "goerli"
	PulseChainTestnetV3Name = "pulsechain-testnet-v3"
	RopstenName             = "ropsten"
	SepoliaName             = "sepolia"
)
